{
   'targets' : [
      {
        'target_name': 'converter',
        'includes' : [
        	'common.gypi',
        ],
        'type' : '<(common_build_type)',
        'sources' : [
            '<(DEPTH)/src/converter.h',
            '<(DEPTH)/src/converter.cc',
        ],
      },
   ],
}