{
	'targets' : [
      {
        'target_name': 'computer',
        'includes' : [
        	'common.gypi',
        ],
        'type' : '<(common_build_type)',
        'sources' : [
        	'<(DEPTH)/src/computer.h',
        	'<(DEPTH)/src/computer.cc',
        ],
      },
    ],
}