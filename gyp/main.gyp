 {
    'targets' : [
      {
        'target_name': 'main',
        'type' : 'executable',
        'includes' : [
            'common.gypi',
        ],
        'dependencies' : [
            #'tool.gyp:tool',
        ],

        'link_settings' : {
            'conditions' : [
                [ 'depend_build_type == "shared_library"' , {
                    'libraries' : [
                        'tool.dll',
                        #'shared.dll',
                    ],
                    }, {
                        'libraries' : [
                            'obj/gyp/tool.lib',
                        ],
                    },
                ],
            ],
        },

        'include_dirs' : [
        	'.',
        	'./src',
        ],
        'sources' : [
        	'<(DEPTH)/src/main.cc',
        ],
      },
    ],
  }