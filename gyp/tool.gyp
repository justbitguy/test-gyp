{
	'targets' : [
      {
        'target_name': 'tool',
        'includes' : [
            'common.gypi',
        ],

        'type' : '<(depend_build_type)',
        'dependencies' : [
        	#'computer.gyp:computer', 
        	#'util.gyp:util', 
        	#'converter.gyp:converter',
        ],
        'link_settings' : {
            'conditions' : [
                [ 'common_build_type == "shared_library"' , {
                    'libraries' : [
                        'util.dll',
                        'computer.dll',
                        'converter.dll',
                    ],
                    }, {
                        'libraries' : [
                            'obj/gyp/util.lib',
                            'obj/gyp/computer.lib',
                            'obj/gyp/converter.lib',
                        ],
                    },
                ],
            ],
        },

        'sources' : [
        	'<(DEPTH)/src/tool.h',
        	'<(DEPTH)/src/tool.cc',
        ],
      },
    ],
}