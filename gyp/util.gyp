{
    'targets': [
      {
        'target_name': 'util',
        'includes' : [
        	'common.gypi',
        ],
        'type' : '<(common_build_type)',
        'sources' : [
        	'<(DEPTH)/src/util.h',
        	'<(DEPTH)/src/util.cc',
        ],
      },
    ],
}