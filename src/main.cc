#include <windows.h>

#include "tool.h"
#include "util.h"
#include "computer.h"
#include "stdio.h"

int main()
{
	Tool* t = new Tool(); 
	int r0 = t->handle(); 
	t->callTest();

	Computer* c = new Computer(); 

	float a = 1.0; 
	float b = 2.0;
	float r1 = c->add(a, b); 
	float r2 = c->sub(a, b);
	float r3 = c->mul(a, b); 
	float r4 = c->div(a, b); 
	 
	Util* u = new Util(); 
	u->print(r0); 
	u->print(r1); 


	// delete t;
	delete c; 
	delete u; 

	// to keep the window
	Sleep(10000);

	return 0;  
}