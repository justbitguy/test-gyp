#include "util.h"

#include "stdio.h"

void Util::print(const char* msg)
{
	printf("### %s\n", msg); 
}

void Util::print(const int n)
{
	printf("### %d\n", n); 
}